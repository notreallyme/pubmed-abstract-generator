# PubMed Abstract Generator

Download the entire PubMed database, extract the abstracts in English and train an RNN to create 'new' ones.
