#!/usr/bin//python

# Download the complete Pubmed database.
# This script takes as input the chosen target directory.
# It will connect with the PubMed server, identify which files have not been downloaded, 
# and download them.
# WARNING: Before unzipping, the full download is approximately 25 GB.
# After unzipping it is nearer .25 TB!

# Correct useage:
# ./download_pubmed.py /Users/JoeBloggs/PubMed

# To unzip in bash:
# gunzip -dk ~/pubmed_download/*.gz\n",

import urllib
import argparse
import os
from ftplib import FTP

def main(target_dir):

    # paths for NCBI ftp server
    ROOT_PATH = "ftp://ftp.ncbi.nlm.nih.gov/pubmed/baseline/"
    FILE_STEM = "pubmed18n" # 2018 database
    FILE_END = ".xml.gz"

    def pad_left(file_count, number_chars=4):
        count_length = len(str(file_count))
        if (count_length > number_chars):
            raise ValueError("Number of digits in the integer must be less than the desired number of characters")
        padding = "0" * (number_chars - count_length)
        return padding + str(file_count)

    # login to the NCBI FTP server and list the available files 
    from ftplib import FTP
    print("Accessing NCBI ftp server...")
    ftp = FTP('ftp.ncbi.nlm.nih.gov')     # connect to host, default port
    ftp.login()
    print("Available files in this location:")
    dir_contents = []
    def append_line(line):
        dir_contents.append(line)
        
    ftp.cwd('pubmed/baseline/')
    ftp.retrlines('LIST', callback=append_line)
    print(dir_contents)
    ftp.quit()

    files_in_remote_dir = [line.split()[-1] for line in dir_contents if line.endswith(".gz")]

    # create a destination directory if needed 

    destination_directory = target_dir
    if not os.path.isdir(destination_directory):
        os.mkdir(destination_directory)
    files_in_local_dir = os.listdir(destination_directory)

    # which files don't we have?
    missing_files = [file for file in files_in_remote_dir if file not in files_in_local_dir]


    # now download those missing
    ftp = FTP('ftp.ncbi.nlm.nih.gov')
    ftp.login()
    ftp.set_pasv(False)
    ftp.cwd('pubmed/baseline/')

    missing_files = [file for file in files_in_remote_dir if file not in files_in_local_dir]

    for i in range(928):
        file_name = FILE_STEM + pad_left(i+1) + FILE_END
        destination_file = destination_directory + "/" + file_name
        print ("Retrieving " + file_name)
        print ("Saving to " + destination_file)
        ftp.retrbinary('RETR ' + file_name, open(destination_file, 'wb').write)

    ftp.quit()
    print("Download complete.")

parser = argparse.ArgumentParser()
parser.add_argument("target_dir")
args = parser.parse_args()
if __name__ == '__main__':
    main(args.target_dir)
