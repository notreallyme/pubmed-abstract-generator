#!/usr/bin//python
# This script takes a downloaded, unzipped archive from PubMed,
# and extracts all the English language abstracts.
# If run as a standalone, it simply prints them to stdout.

import xml.etree.ElementTree as ET
import argparse

def get_english_abstracts(file):
    tree = ET.parse(file)
    xml_root = tree.getroot()
    abstracts = []
    for article in xml_root:
        if article.find(".//Language").text == "eng":
            abstract = article.find(".//AbstractText")
            if abstract != None:
                abstracts.append(abstract.text)
    return(abstracts)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    args = parser.parse_args()
    print("Parsing .xml")
    print("This will take a few moments, please be patient.")
    abstracts = get_english_abstracts(args.file)
    print(abstracts)
